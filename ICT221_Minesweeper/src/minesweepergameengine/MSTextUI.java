/**
 *
 */
package minesweepergameengine;

import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * @author blakewarren
 *
 */
public class MSTextUI {
	private Scanner in;
	private PrintStream out;

	public MSTextUI(Scanner input, PrintStream output) {
		this.in = input;
		this.out = output;
	}

	//have to add time
	public void playMSGame(Minesweeper game) {
		game.SetRboard();
		game.PopulateBoard();
		System.out.println("Welcome to Minesweeper");
		System.out.println("There are 10 mines to be found");
		while (game.winner() == 0){
			System.out.println();
			String[] rows = game.display();
			for (String r : rows) {
				System.out.println(r);
			}
			boolean good = false;
			boolean markgood = false;
			String marking = " ";
			int row = 0;
			int col = 0;
			try {
				out.printf("Player Turn...\n");
				out.printf("Do you wish to mark a mine? (y/n)");
				marking = in.toString();
				if (marking.equals("y")){
					markgood = true;
					out.printf("Please indicate where you think a mine is./n");
				} else {
					out.printf("Please indicate where you don't think a mine is./n");
				}
				out.printf("     row (1..10)? ");
				row = in.nextInt();
				out.printf("  column (1..10)? ");
				col = in.nextInt();
				if (1 <= row && row <= 10 && 1 <= col && col <= 10) {
					good = true;
				}

			} catch (InputMismatchException ex) {
				good = false;
				in.nextLine(); // skip the bad input
			} catch (NoSuchElementException ex) {
				good = false;
			}
			if (markgood){
				if (good){
					good = game.markMove(row - 1, col - 1);
				} else {
					out.println("Bad move, try again...");
				}
			} else {
				if (good){
					if (game.IsMine(row, col) != true){
						good = game.move(row - 1, col - 1);
					} else {
						System.out.println("OH NO! You struck a mine.\n");
						System.out.println("Game Over!");
						System.out.println();
						String[] endrows = game.enddisplay();
						for (String r : endrows) {
							System.out.println(r);
						}
						game.gameisLost();
					}
				} else {
					out.println("Bad move, try again...");
				}
			}
		}
		out.println("Congratulations! You found all the mines!");
		System.out.println();
		String[] endrows = game.enddisplay();
		for (String r : endrows) {
			System.out.println(r);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MSTextUI ui = new MSTextUI(new Scanner(System.in), System.out);
		Minesweeper game = new Minesweeper();
		ui.playMSGame(game);
	}

}
