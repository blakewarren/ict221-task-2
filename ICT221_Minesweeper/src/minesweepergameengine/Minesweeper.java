/**
 *
 */
package minesweepergameengine;

/**
 * @author blakewarren
 *
 */
public class Minesweeper {
	private int [][] board = new int [10][10];
	private int [][] rboard = new int [10][10];
	private int minesleft = 10;


	public void SetRboard(){
		for (int row = 0; row < 10; row++){
			for (int col = 0; col < 10; col++){
				rboard[row][col] = 0;
			}
		}
	}

	public void PopulateBoard(){
		int i = 0;
		while (i < 10){
			int row = (int) (Math.random() * 10);
			int col = (int) (Math.random() * 10);
			if (board[row][col] != 99){
				board[row][col] = 99;
				i++;
			}
		}
		for (int row = 0; row < 10; row++){
			for (int col = 0; col < 10; col++){
				if (IsMine(row, col) == false){
					board[row][col] = CountMines(row, col);
				}
			}
		}
	}

	public boolean move(int row, int col){
		if (IsRevealed(row, col) != 0){
			return false;
		}
		rboard[row][col] = 2;
		return true;
	}

	public boolean IsMine (int row, int col){
		return board[row][col] == 99;
	}

	public int IsRevealed (int row, int col){
		return rboard[row][col];
	}

	public int getCell(int row, int col) {
		return board[row][col];
	}

	public int winner() {
		if (minesleft == 0){
			return 1;
		}
		return 0;
	}

	public boolean markMove(int row, int col) {
		if (IsRevealed(row, col) == 0){
			rboard[row][col] = 1;
			if (IsMine(row, col)){
				minesleft = minesleft - 1;
			}
			return true;
		} else if (IsRevealed(row, col) == 1){
			rboard[row][col] = 0;
			if (IsMine(row, col)){
				minesleft = minesleft + 1;
			return true;
			}
		}
		return false;
	}

	public void gameisLost() {
		minesleft = 0;

	}

	public int CountMines (int row, int col){
		int count = 0;
		if (row > 0 & row < 9){
			for (int checkrow = row - 1; checkrow < row + 2; checkrow++){
				if (col > 0 & col < 9){
					for (int checkcol = col - 1; checkcol < col + 2; checkcol++){
						if (IsMine(checkrow,checkcol)){
							count++;
						}
					}
				} else if (col == 0){
					for (int checkcol = col; checkcol < col + 2; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				} else if (col == 9){
					for (int checkcol = col - 1; checkcol < col + 1; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				}
			}
		} else if (row == 0){
			for (int checkrow = row; checkrow < row + 2; checkrow++){
				if (col > 0 & col < 9){
					for (int checkcol = col - 1; checkcol < col + 2; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				} else if (col == 0){
					for (int checkcol = col; checkcol < col + 2; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				} else if (col == 9){
					for (int checkcol = col - 1; checkcol < col + 1; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				}
			}
		} else if (row == 9){
			for (int checkrow = row - 1; checkrow < row + 1; checkrow++){
				if (col > 0 & col < 9){
					for (int checkcol = col - 1; checkcol < col + 2; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				} else if (col == 0){
					for (int checkcol = col; checkcol < col + 2; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				} else if (col == 9){
					for (int checkcol = col - 1; checkcol < col + 1; checkcol++){
						if (IsMine(checkrow,checkcol) == true){
							count++;
						}
					}
				}
			}
		}
		return count;
	}

	public String[] display() {
		String lines[] = new String[10];
		for (int row = 0; row < 10; row++) {
			// A StringBuilder is like a String, but with update methods.
			StringBuilder str = new StringBuilder();
			for (int col = 0; col < 10; col++) {
				if (IsRevealed(row, col) == 2){
					int cell = getCell(row, col);
					if (cell == 0) {
						str.append(" .");
					} else if (cell == 99){
						str.append(" *");
					} else {
						str.append(" " + cell);
					}
				} else if (IsRevealed(row, col) == 1){
					str.append(" m");
				} else {
					str.append(" +");
				}
			}
			lines[row] = str.toString();
		}
		return lines;
	}

	public String[] enddisplay() {
		String lines[] = new String[10];
		for (int row = 0; row < 10; row++) {
			// A StringBuilder is like a String, but with update methods.
			StringBuilder str = new StringBuilder();
			for (int col = 0; col < 10; col++) {
				int cell = getCell(row, col);
				if (cell == 0) {
					str.append(" .");
				} else if (cell == 99){
					str.append(" *");
				} else {
					str.append(" " + cell);
				}
			}
			lines[row] = str.toString();
		}
		return lines;
	}


}
